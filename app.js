const http = require('http');
const fs = require('fs');

let data = [
    {
        id : 1,
        content : 'Nodejs',
        date : new Date()
    },
    {
        id : 2,
        content : 'Javascript',
        date : new Date()
    },
    {
        id : 3,
        content : 'C++',
        date : new Date()
    }
]

const app = http.createServer(function(req, res){
    console.log('made request');

    //home page
    if(req.url === '/home' || req.url === '/'){
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('This is home page');
    }
    
    //fetching content form html
    else if(req.url === '/about'){
        res.writeHead(200, { 'Content-Type': 'text/html' });
        fs.createReadStream(__dirname + '/about.html').pipe(res);
    } 

    //display in json
    else if(req.url === '/api'){
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(data))
    } 
    
    //Give error for undefined route
    else{
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end('404 Not found');
    }
});

// const app = http.createServer((req, res) => {
//   res.writeHead(200, { 'Content-Type': 'text/plain' })
//   res.end('Hello World')
// })

const port = 3001
app.listen(port)
console.log(`Server running on port ${port}`)